#!/usr/bin/python
# -*- coding: utf-8 -*-
import ConfigParser
import os
import subprocess
import time
import shutil
import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

config = ConfigParser.ConfigParser()
config.read("mysql.conf")
username = config.get('client', 'user')
password = config.get('client', 'password')
hostname = config.get('client', 'host')
outofdate = config.get('client', 'days')
dumpdir = config.get('client', 'dumpdir')
frommail = config.get('client', 'fromaddr')
tomail = config.get('client', 'toaddr')
mailpass = config.get('client', 'mailpassword')
logsdir = config.get('client', 'logsdir')

filestamp = time.strftime('%Y-%m-%d')

try:
    subprocess.call(['mkdir', '{0}'.format(logsdir)])
    print "Creation succesful"
except (OSError, ValueError):
    print "Log directory exist"
    pass

logging.basicConfig(format=u'%(levelname)-8s [%(asctime)s] %(message)s', level=logging.DEBUG,
                    filename=(logsdir + filestamp))

# os.system ("mkdir -p %s/%s" % (dumpdir, filestamp))

try:
    subprocess.call(['mkdir', '-p', '{0}/{1}'.format(dumpdir, filestamp)])
    logging.info(u'Dump directory created: ' + '{0}/{1}'.format(dumpdir, filestamp))
except OSError:
    logging.critical(u'Can not create dump directory!!!')
    pass
# Get a list of databases with :
database_list_command = "mysql -u %s -p%s -h %s --silent -N -e 'show databases'" % (username, password, hostname)
for database in os.popen(database_list_command).readlines():
    database = database.strip()
    if database == 'information_schema':
        continue
    if database == 'performance_schema':
        continue
    os.system("mkdir -p %s/%s/%s" % (dumpdir, filestamp, database))
    all_tables = "mysql -u %s -p%s -h %s --silent -N -e 'show tables' %s" % (username, password, hostname, database)
    for tables in os.popen(all_tables).readlines():
        tables = tables.strip()
        filename = "%s/%s/%s/%s.sql" % (dumpdir, filestamp, database, tables)
        try:
            backup = subprocess.call(
                ['mysqldump', '-alv', '--opt', '--lock-tables=0', '-u{0}'.format(username), '-p{0}'.format(password),
                 '-h{0}'.format(hostname), '{0}'.format(database), '{0}'.format(tables), '-r{0}'.format(filename)])
            logging.debug(backup)
            bodymail = "Dump successful, logs in attachments!"
        except (OSError, ValueError):
            logging.critical(u'Backup going wrong!!!')
            bodymail = "Dump fail, logs in attachments good luck!"
            break


def deold():
    numdays = 86400 * float(outofdate)
    now = time.time()
    directory = os.path.join('%s' % (dumpdir))
    for r, d, f in os.walk(directory):
        for dir in d:
            timestamp = os.path.getmtime(os.path.join(r, dir))
            if now - numdays > timestamp:
                try:
                    logging.info(u'Removing: ' + os.path.join(r, dir))
                    shutil.rmtree(os.path.join(r, dir))
                except Exception, e:
                    logging.error(u'Problem delete old dirs')
                    pass
                else:
                    print "some message for success"


deold()


def sendmail():
    msg = MIMEMultipart()
    msg['From'] = "{0}".format(frommail)
    msg['To'] = "{0}".format(tomail)
    msg['Subject'] = "Backup %s" % (filestamp)
    # body = "Logs in attachment"
    msg.attach(MIMEText(bodymail, 'plain'))
    attachment = open("{0}/{1}".format(logsdir, filestamp), "rb")
    p = MIMEBase('application', 'octet-stream')
    p.set_payload((attachment).read())
    encoders.encode_base64(p)
    p.add_header('Content-Disposition', "attachment; filename= %s" % filestamp)
    msg.attach(p)
    s = smtplib.SMTP('smtp.gmail.com', 587)
    s.starttls()
    s.login("{0}".format(frommail), "{0}".format(mailpass))
    text = msg.as_string()
    s.sendmail("{0}".format(frommail), "{0}".format(tomail), text)
    s.quit()


sendmail()
