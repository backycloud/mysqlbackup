#Скрипт потабличного дампа базы даныых mysql

***Пример запуска: ./backup.py***

1. Бэкапит все БД, кроме information_schema, performance_schema
2. Ротация бэкапов
3. Логирование
4. Уведомление на почту + логи.


#Все конфигурационные данные в mysql.conf

**Конфигурационные параметры для mysql.**
***host***
***user***
***password***

**Директория с бэкапами, если её не существует она будет создана**
***dumpdir***

**Директория с логами**
***logsdir***

**Кол-во дней, для ротации бэкапов**
***days***

**Данные для почты**
***fromaddr***
***toaddr***
***mailpassword***

